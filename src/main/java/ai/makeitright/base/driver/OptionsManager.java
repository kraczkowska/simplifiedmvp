package ai.makeitright.base.driver;

import org.openqa.selenium.chrome.ChromeOptions;

public class OptionsManager {
    public static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        return options;
    }
}
