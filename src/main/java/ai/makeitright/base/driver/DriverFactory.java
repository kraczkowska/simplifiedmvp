package ai.makeitright.base.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

import java.io.File;

public class DriverFactory {
    public static WebDriver createWebDriverInstance(String browserName) {
        WebDriver driver = null;
        if(browserName.equalsIgnoreCase("chrome")) {
            if(System.getProperty("webdriver.chrome.driver") == null) {
                File iDriver = new File(System.getProperty("user.dir")+"/src/main/resources/webdrivers/chromedriver.exe");
                System.setProperty("webdriver.chrome.driver",iDriver.getAbsolutePath());
                driver = new ChromeDriver(OptionsManager.getChromeOptions());
            }
        }
        return driver;
    }
}
