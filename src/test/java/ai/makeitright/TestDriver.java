package ai.makeitright;

import ai.makeitright.base.driver.DriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class TestDriver {

    private WebDriver driver;

    @Before
    public void before() {
        DriverManager.createInstance("chrome");
        driver = DriverManager.getDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void test1() {
        driver.get("https://www.onet.pl");
    }

    @After
    public void after() {
        driver.close();
    }

}
